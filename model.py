from process_fightmetric import FightmetricDataContext
import pandas as pd
from datetime import date
from sklearn.ensemble import RandomForestRegressor

scoring = {
    'strikes': 0.5,
    'takedowns': 2,
    'submissions': 1,
    'passes': 1,
    'round_1_win': 100,
    'round_2_win': 70,
    'round_3_win': 50,
    'round_4_win': 40,
    'round_5_win': 40,
    'win_by_decision': 25
}

def calc_score(result):
    f1_result = result['strikes_1'] * scoring['strikes'] + \
                result['takedowns_1'] * scoring['takedowns'] + \
                result['submissions_1'] * scoring['submissions'] + \
                result['passes_1'] + scoring['passes']

    f2_result = result['strikes_2'] * scoring['strikes'] + \
                result['takedowns_2'] * scoring['takedowns'] + \
                result['submissions_2'] * scoring['submissions'] + \
                result['passes_2'] + scoring['passes']

    # Fight Results
    win_adj = 0
    if result['round'] == 1:
        win_adj = scoring['round_1_win']
    elif result['round'] == 2:
        win_adj = scoring['round_2_win']
    elif result['round'] == 3:
        win_adj = scoring['round_3_win']
    elif result['round'] == 4:
        win_adj = scoring['round_4_win']
    elif result['round'] == 5:
        win_adj = scoring['round_5_win']
    elif '-DEC' in result['method']:
        win_adj = scoring['win_by_decision']

    if result['result_1'] == 'win':
        f1_result += win_adj
    if result['result_2'] == 'win':
        f2_result += win_adj

    return (f1_result, f2_result)


dc = FightmetricDataContext('data_files/items_result_spider_6.json')

df = pd.DataFrame(dc.results)
df['fighter_1_id'] = df['fighter_1_url'].str[-16:]
df['fighter_2_id'] = df['fighter_2_url'].str[-16:]

fighterDf = pd.DataFrame(dc.fighters)
fighterDf['fighter_id'] = fighterDf['url'].str[-16:]
df = df.merge(fighterDf, 'left', right_on='fighter_id', left_on='fighter_1_id', suffixes=('','_f1'))
df = df.merge(fighterDf, 'left', right_on='fighter_id', left_on='fighter_2_id', suffixes=('','_f2'))


df['score_f1'] = df.apply(lambda x: calc_score(x)[0],1)
df['score_f2'] = df.apply(lambda x: calc_score(x)[1],1)

trainCutoff = date(2015,1,1)
trainRatio = len(df[df.date <= trainCutoff]) / float(len(df))
print 'train/test ratio:', trainRatio

independentVars = ['SApM','SLpM','StrAcc','StrDef','SubAvg','TDAcc','TDAvg','TDDef','SApM_f2','SLpM_f2','StrAcc_f2','StrDef_f2','SubAvg_f2','TDAcc_f2','TDAvg_f2','TDDef_f2']

def genColDict(cols):
    retDict = {}
    for c in cols:
        if c[-3:] == '_f2':
            retDict[c] = c[:-3]
        else:
            retDict[c] = c + '_f2'
    return retDict

def getModelData(df):
    f1Df = df[independentVars + ['score_f1']].rename(columns={'score_f1': 'score'})
    f2Df = df[independentVars + ['score_f2']].rename(columns=genColDict(independentVars)).rename(columns={'score_f2': 'score'})
    f2Df = f2Df[independentVars + ['score']]
    allDf = f1Df.append(f2Df)

    return allDf


trainData = getModelData(df[(df.date <= trainCutoff) & (df.date >= date(2000,1,1))])
trainDf = trainData[independentVars]
trainResults = trainData.score


testData = getModelData(df[df.date <= trainCutoff])
testDf = testData[independentVars]
testResults = testData.score



model = RandomForestRegressor(200)
model.fit(trainDf, trainResults)
print 'model score:', model.score(testDf, testResults)
print '-----------------------'
importances = zip(model.feature_importances_, independentVars)
importances = sorted(importances, key=lambda x: x[0], reverse=True)
for i in importances:
    print i[1], i[0]

