import pandas as pd
import itertools
import numpy as np
import process_sherdog
from elo import calcELO
import traceback
from datetime import datetime


print 'Processing sherdog data...'

sherdog_data = process_sherdog.SherdogDataContext('data_files/items_event_spider_6.json')
eventData = sherdog_data.events
fightData = sherdog_data.fights
fighterData = sherdog_data.fighters


print 'Calculating ELOs...'

fightersById = {}
for f in fighterData:
    fightersById[int(f['id'])] = f


startTime = datetime.now()

eloRatingsById = {}
for f in fightersById:
    eloRatingsById[f] = 1500

fightsByEvent = {}
for f in fightData:
    eventId = int(f['event_id'])
    if eventId not in fightsByEvent:
        fightsByEvent[eventId] = []
    fightsByEvent[eventId].append(f)

for event in sorted(eventData, key=lambda x: x['date']):
    id = int(event['id'])
    # print event['name'], event['date']
    if id not in fightsByEvent:
        continue
    eventFights = fightsByEvent[id]
    for fight in eventFights:
        try:
            f1 = int(fight['fighter_1_id'])
            f2 = int(fight['fighter_2_id'])

            f1_result = fight['fighter_1_result']
            f2_result = fight['fighter_2_result']

            result = None
            if f1_result == 'win':
                result = 1
            elif f1_result == 'loss':
                result = 2
            elif f1_result == 'draw':
                result = 0

            if result:
                if f1 not in eloRatingsById:
                    eloRatingsById[f1] = 1500

                if f2 not in eloRatingsById:
                    eloRatingsById[f2] = 1500

                ratings = calcELO(eloRatingsById[f1],
                                      eloRatingsById[f2],
                                      result,
                                      k=180)
                eloRatingsById[f1] = ratings[0]
                eloRatingsById[f2] = ratings[1]

        except:
            # traceback.print_exc()
            pass

eloSorted = sorted([x for x in eloRatingsById.iteritems()], reverse=True, key=lambda x: x[1])

topToShow = 50

for x in range(0,topToShow):
    print fightersById[eloSorted[x][0]]['name'] , eloSorted[x][1]

with open('data_files/eloRatingsById.csv', 'w') as f:
    for k, v in eloRatingsById.iteritems():
        f.write(str(k) + ',' + str(v))
        f.write('\n')


print 'processing took: {0}'.format((datetime.now() - startTime))

print 'applying ELOs to fighters...'

path = r'data_files/DKSalaries_FN85.csv'

df = pd.read_csv(path, index_col=None)

df['fighterId'] = df.Name.apply(lambda x: [y[0] for y in fightersById.iteritems() if y[1]['name'] == x])

df.fighterId = df.fighterId.apply(lambda x: np.nan if len(x) == 0 else x[0])

df['elo'] = df.fighterId.apply(lambda x: eloRatingsById[x] if not np.isnan(x) else np.nan)

df.elo.loc[np.isnan(df.elo)] = 1500


def getSum(combo, numElements=5):
    sum = 0
    for i in range(numElements):
        sum += combo[i][2]
    return sum

def duplicateMatch(combo, numElements=5):
    matches = []
    for i in range(numElements):
        matches.append(combo[i][3])
    return len(set(matches)) < len(matches)



df['pvp'] = df.apply(lambda x: x.GameInfo.split(' ')[0].split('@'), 1)
df['opponent'] = df.apply(lambda x: x.pvp[1] if x.pvp[0] == x.teamAbbrev else x.pvp[0],1)
eloMap = df[['teamAbbrev','elo']]
eloMap = eloMap.set_index('teamAbbrev')
eloMap = eloMap.to_dict()['elo']
df['oppElo'] = df.apply(lambda x: eloMap[x.opponent] , 1)
df['eloDiff'] = df['elo'] - df['oppElo']

print 'calculating permutations of fighters...'

eloTuples = []
for t in df[['Name','fighterId','Salary','GameInfo','eloDiff','elo']].itertuples(index=False):
    eloTuples.append(t)

gen = itertools.permutations(eloTuples, 5)
eloCombos = []
for c in gen:
    eloCombos.append(sorted(c))

eloCombos.sort()
eloCombos = list (k for k,_ in itertools.groupby(eloCombos))


under50k = [x for x in eloCombos if getSum(x) <= 50000]
under50kNoDups = [x for x in under50k if not duplicateMatch(x)]


maxElo = map(lambda x: reduce(lambda a, b: a + b[4], x, 0), under50kNoDups)

i = zip(maxElo, under50kNoDups)
mElo = max(maxElo)
maxedOut = [x for x in i if x[0] == mElo]

print df
print pd.DataFrame(maxedOut[0][1])


