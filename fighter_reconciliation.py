from process_fightmetric import FightmetricDataContext
from process_sherdog import SherdogDataContext

fdc = FightmetricDataContext('data_files/items_result_spider_6.json')
sdc = SherdogDataContext('data_files/items_event_spider_6.json')

def clean_name(name):
	retName = name.replace('.','')
	retName = retName.replace('-','')
	retName = retName.replace('\'','')
	retName = retName.replace(' ','')
	retName = retName.upper()

	return retName

fm_names = [clean_name(x['name']) for x in fdc.fighters]
sd_names = [clean_name(x['name']) for x in sdc.fighters]



fm_uniques = set(fm_names)
sd_uniques = set(sd_names)

fm_in_sd = [x for x in fm_uniques if x in sd_uniques]

fm_not_in_sd = [x for x in fm_uniques if x not in sd_uniques]