# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class Event(scrapy.Item):
    id = scrapy.Field()
    date = scrapy.Field()
    name = scrapy.Field()
    location = scrapy.Field()
    url = scrapy.Field()


class Fight(scrapy.Item):
    event_id = scrapy.Field()
    match = scrapy.Field()
    fighter_1_result = scrapy.Field()
    fighter_1_url = scrapy.Field()
    fighter_2_result = scrapy.Field()
    fighter_2_url = scrapy.Field()
    method = scrapy.Field()
    round = scrapy.Field()
    time = scrapy.Field()
    referee = scrapy.Field()


class Fighter(scrapy.Item):
    id = scrapy.Field()
    name = scrapy.Field()
    birthday = scrapy.Field()
    hometown = scrapy.Field()
    country = scrapy.Field()
    height_ft = scrapy.Field()
    weight = scrapy.Field()
    wclass = scrapy.Field()
    winLoss = scrapy.Field()
    methods = scrapy.Field()


class Result(scrapy.Item):
    name = scrapy.Field()
    date = scrapy.Field()
    location = scrapy.Field()
    attendance = scrapy.Field()
    fighter_1 = scrapy.Field()
    fighter_1_url = scrapy.Field()
    result_1 = scrapy.Field()
    strikes_1 = scrapy.Field()
    takedowns_1 = scrapy.Field()
    submissions_1 = scrapy.Field()
    passes_1 = scrapy.Field()
    fighter_2 = scrapy.Field()
    fighter_2_url = scrapy.Field()
    result_2 = scrapy.Field()
    strikes_2 = scrapy.Field()
    takedowns_2 = scrapy.Field()
    submissions_2 = scrapy.Field()
    passes_2 = scrapy.Field()
    wclass = scrapy.Field()
    method = scrapy.Field()
    round = scrapy.Field()
    time = scrapy.Field()


class FmFighter(scrapy.Item):
    url = scrapy.Field()
    id = scrapy.Field()
    name = scrapy.Field()
    record = scrapy.Field()
    nickname = scrapy.Field()
    height = scrapy.Field()
    weight = scrapy.Field()
    reach = scrapy.Field()
    stance = scrapy.Field()
    birthday = scrapy.Field()
    SLpM = scrapy.Field()
    StrAcc = scrapy.Field()
    SApM = scrapy.Field()
    StrDef = scrapy.Field()
    TDAvg = scrapy.Field()
    TDAcc = scrapy.Field()
    TDDef = scrapy.Field()
    SubAvg = scrapy.Field()




