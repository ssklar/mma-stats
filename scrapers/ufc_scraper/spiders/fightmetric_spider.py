import scrapy
from ufc_scraper.items import Result, FmFighter

class ResultSpider(scrapy.Spider):
    base_url = 'http://www.sherdog.com'
    name = 'result_spider'
    allowed_domains = ['fightmetric.com']
    start_urls = [
        'http://fightmetric.com/statistics/events/completed?page=all'
        ]

    def parse(self, response):
        for event in response.css('.b-link').xpath('@href').extract():
            yield scrapy.Request(event, callback=self.parse_event)


    def parse_event(self, response):
        for fight in response.xpath('//table/tbody/tr'):
            result = Result()

            result['name'] = response.css('.b-content__title-highlight').xpath('text()[1]').extract()
            result['date'] = response.css('.b-list__info-box').xpath('ul/li[1]/text()[2]').extract()
            result['location'] = response.css('.b-list__info-box').xpath('ul/li[2]/text()[2]').extract()
            result['attendance'] = response.css('.b-list__info-box').xpath('ul/li[3]/text()[2]').extract()


            result['result_1'] = fight.xpath('td[1]/p[1]').css('.b-flag__text').xpath('text()').extract()
            result['result_2'] = fight.xpath('td[1]/p[2]').css('.b-flag__text').xpath('text()').extract()
            result['fighter_1'] = fight.xpath('td[2]/p[1]/a/text()').extract()
            result['fighter_1_url'] = fight.xpath('td[2]/p[1]/a/@href').extract()
            result['fighter_2'] = fight.xpath('td[2]/p[2]/a/text()').extract()
            result['fighter_2_url'] = fight.xpath('td[2]/p[2]/a/@href').extract()
            result['strikes_1'] = fight.xpath('td[3]/p[1]/text()').extract()
            result['strikes_2'] = fight.xpath('td[3]/p[2]/text()').extract()
            result['takedowns_1'] = fight.xpath('td[4]/p[1]/text()').extract()
            result['takedowns_2'] = fight.xpath('td[4]/p[2]/text()').extract()
            result['submissions_1'] = fight.xpath('td[5]/p[1]/text()').extract()
            result['submissions_2'] = fight.xpath('td[5]/p[2]/text()').extract()
            result['passes_1'] = fight.xpath('td[6]/p[1]/text()').extract()
            result['passes_2'] = fight.xpath('td[6]/p[2]/text()').extract()
            result['wclass'] = fight.xpath('td[7]/p/text()[1]').extract()
            result['method'] = fight.xpath('td[8]/p[1]/text()').extract()
            result['round'] = fight.xpath('td[9]/p[1]/text()').extract()
            result['time'] = fight.xpath('td[10]/p[1]/text()').extract()

            yield result

            if len(result['fighter_1_url']) > 0:
                yield scrapy.Request(result['fighter_1_url'][0], callback=self.parse_fighter)

            if len(result['fighter_2_url']) > 0:
                yield scrapy.Request(result['fighter_2_url'][0], callback=self.parse_fighter)


    def parse_fighter(self, response):
        fighter = FmFighter()
        fighter['url'] = response.url
        fighter['id'] =response.url.split('/')[-1]
        fighter['name'] = response.css('.b-content__title-highlight').xpath('text()').extract()
        fighter['record'] = response.css('.b-content__title-record').xpath('text()').extract()
        fighter['nickname'] = response.css('.b-content__Nickname').xpath('text()').extract()

        data = response.css('.b-list__box-list-item').xpath('text()[2]').extract()
        fighter['height'] = data[0]
        fighter['weight'] = data[1]
        fighter['reach'] = data[2]
        fighter['stance'] = data[3]
        fighter['birthday'] = data[4]
        fighter['SLpM'] = data[5]
        fighter['StrAcc'] = data[6]
        fighter['SApM'] = data[7]
        fighter['StrDef'] = data[8]
        fighter['TDAvg'] = data[10]
        fighter['TDAcc'] = data[11]
        fighter['TDDef'] = data[12]
        fighter['SubAvg'] = data[13]

        yield fighter



