import scrapy
from ufc_scraper.items import Event, Fight, Fighter

class EventSpider(scrapy.Spider):
    base_url = 'http://www.sherdog.com'
    name = 'event_spider'
    allowed_domains = ['sherdog.com']
    start_urls = [
        'http://www.sherdog.com/events/recent/{0}-page'.format(x)
            for x in range(2,300)
        ]


    def parse(self, response):
        for node in response.xpath('//tr[contains(@onclick,"document.location=")]'):
            url = node.xpath('@onclick[1]').extract()[0]
            url = url.replace("document.location='", '')
            url = url.replace("';", '')
            url = self.base_url + url
            yield scrapy.Request(url, callback=self.parse_event_contents)


    def parse_event_contents(self, response):
        # Parse the event here
        event = Event()
        event['date'] = response.xpath('//meta[contains(@itemprop,"startDate")]').xpath('@content[1]').extract()
        event['name'] = response.xpath('//h1/span[contains(@itemprop,"name")][1]/text()').extract()
        event['location'] = response.xpath('//span[contains(@itemprop,"location")]/text()[1]').extract()
        event['url'] = response.url
        event['id'] = response.url.split('-')[-1]
        yield event


        # First parse main event
        main_event = Fight()
        main_event['event_id'] = event['id']
        main_event['fighter_1_url'] = response.css('.left_side').xpath('a/@href[1]').extract()
        main_event['fighter_1_result'] = response.css('.left_side').xpath('span').css('.final_result').xpath('text()[1]').extract()
        main_event['fighter_2_url'] = response.css('.right_side').xpath('a/@href[1]').extract()
        main_event['fighter_2_result'] = response.css('.right_side').xpath('span').css('.final_result').xpath('text()[1]').extract()

        other_data = response.css('.resume').xpath('tr/td/text()')

        main_event['match'] = other_data[0].extract()
        main_event['method'] = other_data[1].extract()
        main_event['referee'] = other_data[2].extract()
        main_event['round'] = other_data[3].extract()
        main_event['time'] = other_data[4].extract()

        yield main_event

        # Then parse sub events
        for e in response.xpath('//tr[contains(@itemprop,"subEvent")]'):
            sub_event = Fight()
            sub_event['event_id'] = event['id']
            sub_event['fighter_1_url'] = e.css('.text_right').css('.fighter_result_data').xpath('a/@href[1]').extract()
            sub_event['fighter_1_result'] = e.css('.text_right').css('.fighter_result_data').css('.final_result').xpath('text()[1]').extract()
            sub_event['fighter_2_url'] = e.css('.text_left').css('.fighter_result_data').xpath('a/@href[1]').extract()
            sub_event['fighter_2_result'] = e.css('.text_left').css('.fighter_result_data').css('.final_result').xpath('text()[1]').extract()
            sub_event['match'] = e.xpath('td')[0].xpath('text()')[-1].extract().strip()
            sub_event['referee'] = e.xpath('td')[4].xpath('span/text()').extract()[0]
            sub_event['method'] = e.xpath('td')[4].xpath('text()').extract()[0]
            sub_event['round'] = e.xpath('td')[5].xpath('text()').extract()
            sub_event['time'] = e.xpath('td')[6].xpath('text()').extract()

            if ('fighter_1_url' in sub_event):
                yield scrapy.Request(self.base_url+ sub_event['fighter_1_url'][0], callback=self.parse_fighter)

            if ('fighter_2_url' in sub_event):
                yield scrapy.Request(self.base_url+ sub_event['fighter_2_url'][0], callback=self.parse_fighter)
            yield sub_event

    def parse_fighter(self, response):
        fighter = Fighter()
        fighter['id'] = response.url.split('-')[-1]
        fighter['name'] = response.xpath('//h1/span[1]/text()').extract()
        fighter['birthday'] = response.xpath('//span[@itemprop="birthDate"][1]/text()').extract()
        fighter['hometown'] = response.css('.locality').xpath('text()').extract()
        fighter['country'] =response.xpath('//strong[@itemprop="nationality"][1]/text()').extract()
        fighter['height_ft'] = response.css('.height').xpath('strong/text()').extract()
        fighter['weight'] = response.css('.weight').xpath('strong/text()').extract()
        fighter['wclass'] = response.css('.wclass').xpath('strong[1]/text()').extract()

        fighter['winLoss'] = response.css('.card').css('.counter').xpath('text()').extract()

        fighter['methods'] = response.css('.bio_graph').css('.graph_tag').extract()

        yield fighter

