import json
from dateutil.parser import parse as dtparse
import re

class FightmetricDataContext(object):
    def __init__(self, path, logging=False):
        self.logging = logging

        with open(path) as f:
            data = json.load(f)

        self.results = [self.clean_raw_data(d) for d in data if d['_type'] == 'Result']
        self.results = [x for x in self.results if x['round']]

        self.fighters = [self.clean_raw_data(d) for d in data if d['_type'] == 'FmFighter']

    __numericRegex = re.compile(r'\d+', re.IGNORECASE)

    def log_message(self, message):
        if self.logging:
            logger.Logger.debug(message)

    def clean_raw_data(self, result):
        item = {}
        for k, v in result.iteritems():
            if type(v) is list:
                if len(v) > 0:
                    item[k] = v[0]
                else:
                    item[k] = None
            else:
                item[k] = v

            if item[k]:
                item[k] = item[k].strip()

                if k in ['date', 'birthday']:
                    try:
                        item[k] = dtparse(item[k])
                    except:
                        self.log_message('unable to process {0}: {1}'.format(k, item[k]))
                        item[k] = None


                elif k in ['SApM', 'SLpM', 'SubAvg', 'TDAvg', 'round']:
                    try:
                        item[k] = float(item[k])
                    except:
                        self.log_message('unable to process {0}: {1}'.format(k, item[k]))
                        item[k] = None

                elif k in ['passes_1', 'strikes_1', 'submissions_1', 'takedowns_1', 'passes_2', 'strikes_2', 'submissions_2', 'takedowns_2']:
                    try:
                        item[k] = float(item[k])
                    except:
                        self.log_message('unable to process {0}: {1}'.format(k, item[k]))
                        item[k] = 0.0

                elif k in ['TDAcc', 'TDDef', 'StrAcc', 'StrDef']:
                    try:
                        item[k] = float(item[k].replace('%','')) / 100.0
                    except:
                        self.log_message('unable to process {0}: {1}'.format(k, item[k]))
                        item[k] = None

                elif k in ['weight', 'reach']:
                    try:
                        item[k] = float(self.__numericRegex.findall(item[k])[0])
                    except:
                        self.log_message('unable to process {0}: {1}'.format(k, item[k]))
                        item[k] = None

        return item
