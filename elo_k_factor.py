import pandas as pd
import itertools
import numpy as np
import process_sherdog
from elo import calcELO
import traceback
from datetime import datetime


k_factors = range(20,301,20)
results = {}

print 'Processing sherdog data...'

sherdog_data = process_sherdog.SherdogDataContext('data_files/items_event_spider_5.json')
eventData = sherdog_data.events
fightData = sherdog_data.fights
fighterData = sherdog_data.fighters


fightsByEvent = {}
for f in fightData:
    eventId = int(f['event_id'])
    if eventId not in fightsByEvent:
        fightsByEvent[eventId] = []
    fightsByEvent[eventId].append(f)


for k in k_factors:

    fightersById = {}
    for f in fighterData:
        fightersById[int(f['id'])] = f


    eloRatingsById = {}
    for f in fightersById:
        eloRatingsById[f] = [1500]


    print 'calculating ELOs for k-factor of', k
    totalFights = 0
    correctPredictions = 0
    for event in sorted(eventData, key=lambda x: x['date']):
        id = int(event['id'])
        # print event['name'], event['date']
        if id not in fightsByEvent:
            continue
        eventFights = fightsByEvent[id]
        for fight in eventFights:

            try:
                f1 = int(fight['fighter_1_id'])
                f2 = int(fight['fighter_2_id'])

                f1_result = fight['fighter_1_result']
                f2_result = fight['fighter_2_result']

                result = None
                if f1_result == 'win':
                    result = 1
                elif f1_result == 'loss':
                    result = 2
                elif f1_result == 'draw':
                    result = 0

                if result:
                    if f1 not in eloRatingsById:
                        eloRatingsById[f1] = [1500]

                    if f2 not in eloRatingsById:
                        eloRatingsById[f2] = [1500]

                    ratings = calcELO(eloRatingsById[f1][-1],
                                          eloRatingsById[f2][-1],
                                          result,
                                          k=k)


                    totalFights += 1
                    if result == 1 and eloRatingsById[f1][-1] > eloRatingsById[f2][-1]:
                        correctPredictions += 1
                    if result == 2 and eloRatingsById[f2][-1] > eloRatingsById[f1][-1]:
                        correctPredictions += 1
                    if result == 0 and eloRatingsById[f1][-1] == eloRatingsById[f2][-1]:
                        correctPredictions += 1

                    eloRatingsById[f1].append(ratings[0])
                    eloRatingsById[f2].append(ratings[1])
            except:
                pass

    results[k] = correctPredictions / float(totalFights)


for k in sorted(results.keys()):
    print k, results[k]
