import json
from dateutil.parser import parse as dtparse
import logger
import re

class SherdogDataContext(object):
    def __init__(self, path, logging=False):
        self.logging = logging

        with open(path) as f:
            data = json.load(f)

        self.events = [self.clean_raw_data(x) for x in data if x['_type'] == 'Event']
        self.fights = [self.clean_raw_data(x) for x in data if x['_type'] == 'Fight']
        self.fighters = [self.clean_raw_data(x) for x in data if x['_type'] == 'Fighter']

    __numericRegex = re.compile(r'\d+', re.IGNORECASE)

    def log_message(self, message):
        if self.logging:
            logger.Logger.debug(message)


    def clean_raw_data(self, result):
        item = {}
        for k, v in result.iteritems():
            if type(v) is list:
                if len(v) > 0:
                    item[k] = v[0]
                else:
                    item[k] = None
            else:
                item[k] = v

            if item[k]:
                if k in ['date','birthday']:
                    try:
                        item[k] = dtparse(item[k])
                    except:
                        self.log_message('unable to process {0}: {1}'.format(k, item[k]))
                        item[k] = None

                elif k in ['draws', 'losses', 'wins', 'event_id','id','fighter_1_id','fighter_2_id','match','round']:
                    try:
                        item[k] = float(item[k])
                    except:
                        self.log_message('unable to process {0}: {1}'.format(k, item[k]))
                        item[k] = None

                elif k in ['weight']:
                    try:
                        item[k] = float(self.__numericRegex.findall(item[k])[0])
                    except:
                        self.log_message('unable to process {0}: {1}'.format(k, item[k]))
                        item[k] = None

                elif k == 'methods':
                    item['wins_by_submission'] = self.clean_method(v[0])
                    item['wins_by_decision'] = self.clean_method(v[1])
                    item['wins_by_knockout'] = self.clean_method(v[2])
                    item['losses_by_submission'] = self.clean_method(v[3])
                    item['losses_by_decision'] = self.clean_method(v[4])
                    item['losses_by_knockout'] = self.clean_method(v[5])

                # Uses v instead of item[k] because the value actually should be a list
                elif k == 'winLoss':
                    if len(v) >= 1:
                        try:
                            item['wins'] = float(v[0])
                        except:
                            self.log_message('unable to process winLoss (wins): {0}'.format(v[0]))
                            item['wins'] = None
                    else:
                        item['wins'] = '0'

                    if len(v) >= 2:
                        try:
                            item['losses'] = v[1]
                        except:
                            self.log_message('unable to process winLoss (losses): {0}'.format(v[1]))
                            item['losses'] = None
                    else:
                        item['losses'] = '0'

                    if len(v) >= 3:
                        try:
                            item['draws'] = v[2]
                        except:
                            self.log_message( 'unable to process winLoss (draws): {0}'.format(v[2]))
                            item['draws'] = None
                    else:
                        item['draws'] = '0'

                elif k == 'fighter_1_url':
                    try:
                        item['fighter_1_id'] = float(item[k].split('-')[-1])
                    except:
                        self.log_message( 'unable to process fighter_1_url to id: {0}'.format(item[k]))
                        item['fighter_1_id'] = None

                elif k == 'fighter_2_url':
                    try:
                        item['fighter_2_id'] = float(item[k].split('-')[-1])
                    except:
                        self.log_message( 'unable to process fighter_2_url to id: {0}'.format(item[k]))
                        item['fighter_2_id'] = None

        return item


    def clean_method(self, method):
        methodRe = re.compile(r'(\d+) ((DECISIONS)|(SUBMISSIONS)|(KO/TKO))')
        matches = methodRe.findall(method)
        val = None
        if len(matches) > 0:
            val = matches[0][0]
            try:
                return float(val)
            except:
                self.log_message( 'unable to process method: {0}'.format(method))
                val = None

