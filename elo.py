def calcELO(player1, player2, outcome, k=24):

    R1 = 10**(player1/400.0)
    R2 = 10**(player2/400.0)


    E1 = R1 / (R1 + R2)
    E2 = R2 / (R1 + R2)

    # Draw
    if outcome == 0:
        S1 = 0.5
        S2 = 0.5

    # Player 1 wins
    elif outcome == 1:
        S1 = 1
        S2 = 0

    # Player 2 wins
    elif outcome == 2:
        S1 = 0
        S2 = 1

    r1 = player1 + k * (S1 - E1)
    r2 = player2 + k * (S2 - E2)

    return (int(round(r1)), int(round(r2)))
