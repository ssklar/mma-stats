# README #

### Environment ###

1. Download python 2.7
2. Install pip
3. `pip install virtualenv`
4. `git clone https://ssklar@bitbucket.org/ssklar/mma-stats.git`
5. `cd mma-stats`
6. `virtualenv ./venv`
7. `source ./venv/bin/activate`
8. `pip install pip-tools`
9. `pip-sync`

### Data Files ###
https://drive.google.com/open?id=0B73w_NvfXPW_R1FzempVQnEwS3M

1. Extract the zip archive
2. `mv <PATH_TO_ARCHIVE>/data_files/ .`


### Launch Environment ###

1. `jupyter notebook`
2. open notebooks/environment.ipynb in the jupyter browser
